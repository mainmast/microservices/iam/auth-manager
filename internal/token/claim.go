package token

import (
	"github.com/dgrijalva/jwt-go"
)

//ClaimToken ...
type ClaimToken struct {
	ID        string `json:"id"`
	TokenType string `json:"token_type"`
	jwt.StandardClaims
}
